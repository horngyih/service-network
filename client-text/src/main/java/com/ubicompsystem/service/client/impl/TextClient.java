package com.ubicompsystem.service.client.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.ubicompsystem.service.client.IClient;
import com.ubicompsystem.service.data.MessageData;
import com.ubicompsystem.service.registry.IClientRegistry;

public class TextClient implements IClient {

	public static final String CLIENT_TYPE = "text";
	
	@Autowired
	IClientRegistry defaultClientRegistry;

	public void registerClient() {
		if( defaultClientRegistry != null ) {
			defaultClientRegistry.register( CLIENT_TYPE, this );
		}
	}
	
	public String process(MessageData data) {
		return "TEXT-CLIENT";
	}

}
