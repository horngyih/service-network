package com.ubicompsystem.service.registry;

import com.ubicompsystem.service.client.IClient;

public interface IClientRegistry {
	void register( String type, IClient client );
	IClient resolve( String type );
}
