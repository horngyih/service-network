package com.ubicompsystem.service.client;

import com.ubicompsystem.service.data.MessageData;

public interface IClient {
	String process( MessageData data );
}
