package com.ubicompsystem.service.registry.impl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.ubicompsystem.service.client.IClient;
import com.ubicompsystem.service.registry.IClientRegistry;

public class DefaultClientRegistry implements IClientRegistry {
	Map<String, IClient> clientRegistry;
	
	public void register(String type, IClient client) {
		if( type != null && !"".equals(type.trim()) && client != null ) {
			if( this.clientRegistry == null ) {
				this.clientRegistry = new ConcurrentHashMap<String, IClient>();
			}
			
			this.clientRegistry.put( type, client );
		}
	}
	
	public IClient resolve(String type) {
		if( this.clientRegistry != null ) {
			return this.clientRegistry.get(type);
		}
		return null;
	}
}
